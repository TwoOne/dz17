﻿#include <iostream>
#include <math.h>

class Vector 
{
public:
    Vector()
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    double Length()
    {
        return sqrt(x * x + y * y + z * z);
    }
    void Show()
    {
        std::cout << x << ' ' << y << ' ' << z << '\n';
        std::cout  << "Vectors Length = " << Length;
    }
private:
    double x=1;
    double y=2;
    double z=3;
};

int main()
{
    Vector v;
    v.Show();
}